// Keywords: command file; command line option; LHE file;

// The program is equivalent to main198,
// but it only writes out timing information as a YODA histogram.
// Timing information is only collected for events that completed successfully,
// i.e., with pythia.next() == true.

// Input and output files are specified on the command line, e.g. like
// ./main197.exe main197.yoda main42.cmnd > out
// The main program contains no analysis.
// It therefore "never" has to be recompiled to handle different tasks.

// WARNING: the timing histograms only go up to 100 seconds / event.

#include <chrono>

#include <YODA/Histo1D.h>
#include <YODA/IO.h>

#include "Pythia8/Pythia.h"

// https://www.learncpp.com/cpp-tutorial/timing-your-code/
class Timer
{
private:
  // Type aliases to make accessing nested type easier
  using Clock = std::chrono::steady_clock;
  using Second = std::chrono::duration<double, std::ratio<1>>;

  std::chrono::time_point<Clock> m_beg{Clock::now()};

public:
  void reset()
  {
    m_beg = Clock::now();
  }

  double elapsed() const
  {
    return std::chrono::duration_cast<Second>(Clock::now() - m_beg).count();
  }
};

using namespace Pythia8;

int main(int argc, char *argv[])
{

  // Check that correct number of command-line arguments
  if (argc < 3)
  {
    cerr << " Unexpected number of command-line arguments. \n You are"
         << " expected to provide one output file name and any number of cmnd files. \n"
         << " Program stopped! " << endl;
    return 1;
  }

  // Check that the provided input names correspond to existing files.
  for (int i = 2; i < argc; i++)
  {
    ifstream is(argv[i]);
    if (!is)
    {
      cerr << " Command-line file " << argv[i] << " was not found. \n"
           << " Program stopped! " << endl;
      return 1;
    }
  }

  // Confirm that external files will be used for input and output.
  cout << "\n >>> PYTHIA settings will be read from files ";
  for (int i = 2; i < argc; i++)
  {
    cout << argv[i] << " ";
  }
  cout << " <<< \n >>> timing histogram will be written to file "
       << argv[1] << " <<< \n"
       << endl;

  // Timer.
  Timer timer;
  YODA::Histo1D bigTimerHisto(1000, 0, 100, "big_timer", "Time per event [s]");
  YODA::Histo1D smallTimerHisto(1000, 0, 0.010, "small_timer", "Time per event [s]");
  YODA::Histo1D timerHisto(1000, 0, 1, "timer", "Time per event [s]");

  // Generator.
  Pythia pythia;

  // Read in commands from external file.
  for (int i = 1; i < argc; i++)
  {
    pythia.readFile(argv[i]);
  }

  // Extract settings to be used in the main program.
  int nEvent = pythia.mode("Main:numberOfEvents");
  int nAbort = pythia.mode("Main:timesAllowErrors");

  // Initialization.
  pythia.init();

  // Begin event loop.
  int iAbort = 0;
  for (int iEvent = 0; iEvent < nEvent; ++iEvent)
  {
    timer.reset();

    // Generate event.
    if (!pythia.next())
    {
      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile())
      {
        cout << " Aborted since reached end of Les Houches Event File\n";
        break;
      }
      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort)
        continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }

    // Time it.
    double t = timer.elapsed();
    bigTimerHisto.fill(t);
    smallTimerHisto.fill(t);
    timerHisto.fill(t);
  }
  // End of event loop. Statistics.
  pythia.stat();
  cout << "Average time per event: " << timerHisto.xMean() << " s" << endl;
  cout << "Standard deviation: " << timerHisto.xStdDev() << " s" << endl;
  cout << "Standard error: " << timerHisto.xStdErr() << " s" << endl;

  // Write histograms.
  const vector<YODA::Histo1D *> histos = {&bigTimerHisto, &smallTimerHisto, &timerHisto};
  YODA::write(argv[1], histos);

  // Done.
  return 0;
}
